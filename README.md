# INI Viewer

INI Viewer is a viewer for editing INI configuration files.

Despite its light weight, it provides an effective, direct user interface to
view and edit INI files.

## Features

### View INI file content

With INI Viewer, you can easily look at the content of your INI files. The
sections and keys are shown in a tree view.

![Treeview](readme_images/1.png)

### Editing features

INI Viewer provides a lot of useful tools to edit your INI files, such as
cloning sections of keys.

### Search

You can search for a key, a section or even a content in the document.

![Search Windows](readme_images/search.png)

>	**Coming Soon** : Search with regular expressions

### Free and free

INI Viewer is a program under GNU GPL 3.0 License, and it is free of cost.

### CLI mode
You can use all of the INIViewer features in console line mode, by using
arguments. For example, you can use
`INIViewer -o myfile.ini -s section.key:"value"` to set the `"value"`
value to the `section` section's key `key`.
By using the `-!` option, you'll run the viewer only in command-line mode,
meaning the UI won't be loaded. So to retrive the content of the INI, the
application return it in `stdout`, so you can use it in others commands.

You can get all the availables options by typing `iniv -h` (or `INIViewer -h`
if you are on Windows, because the `iniv` and `inivc` files are in shell), but
here are a short examples of use :

#### Adding a key

In this example, we open a `dogo.ini` INI file, and add a `mykey` key in the
`mydoor` section, with the content `'click'`, which is created if it don't
exists.

	inivc -o dogo.ini -s mydoor.mykey:click

#### Creating INI from scratch and save it in a file

In this example, we create from scratch a INI file with 2 sections of 2 keys,
and save it in a `scratch.ini` file.

	inivc -a section1 -a section2 -s section1.key1:value11 -s section1.key2:value12 -s section2.key1:value21 -s section2.key2:value22 > scratch.ini

Note : you can also avoid the `-a` options, because the sections are
automatiquelly created if needed :

	inivc -s section1.key1:value11 -s section1.key2:value12 -s section2.key1:value21 -s section2.key2:value22 > scratch.ini

Shell note : you can also type a long command in multiples lines by escaping
newlines :

	inivc -s section1.key1:value11 \
	      -s section1.key2:value12 \
	      -s section2.key1:value21 \
	      -s section2.key2:value22 \
	      > scratch.ini

#### Removing a section from an existant INI

In this example, we remove the `sec` section from a `input.ini` file.

	inivc -o input.ini --rs sec

#### Removing a key from a section from an existant INI

In this example, we remove the `key` key of the `sec` section from a
`input.ini` file.

	invic -o input.ini -r sec.key

#### Creating a script to rename keys in multiple INI files

In this example, we create a shell script to rename all `rename_me_senpai` keys
of sections `im_a_section` of each INI files of a folder :

	#! /bin/bash
	# $1 contains the folder
	
	if [ ! -z $1 ]; then
		folder=$1
	else
		folder=.
	fi
	if [ -d $folder ]; then
		for i in $folder/*.ini; do
			inivc -o $folder/$i --rnk im_a_section.rename_me_senpai your_new_name > $folder/$i
		done
	else
		echo 'not a folder'
	fi
	exit 0


## How to use it

Unfortunately, for the moment, there are no compiled executable package for
INI Viewer. In order to use it :

### UNIX/Linux

1) You must have python 3 (at least 3.6) installed with tkinter
2) Clone the git project on your computer (or download it)
3) (optional) If you just want to use the application without modifying it,
   you can remove some useless files :
	* `test.ini`
	* `README.md`
	* `readme_images/*`
4) Make executable the `INIViewer` file at the project root by running
   `chmod +x INIViewer` shell command
5) Make executable the `iniv` and `inivc` files at the project root by
   running `chmod +x iniv; chmod +x inivc` shell command
5) You can run directly the `INIViewer` file, or run the `iniv` file (shorter)
   and the `inivc` file (only cli mode, equivalent to `INIViewer -!`)

### Windows

1) You must have python 3 (at least 3.6) installed with tkinter
2) If you have git installed, clone the git project with the
   `git clone https://gitlab.com/SamayoManati/ini-viewer.git iniviewer`
   command; or just download it from [https://gitlab.com/SamayoManati/ini-viewer](https://gitlab.com/SamayoManati/ini-viewer)
3) (optional) If you just want to use the application without modifying it, you
   can remove some useless files (in your case) :
	* `test.ini`
	* `README.md`
	* `readme_images/*`
	* `iniv`
	* `inivc`
4) (optional) Rename the `INIViewer` file to `INIViewer.py` to launch it by
   double-clicking
5) If not, launch the `INIViewer` file with python in the console

## Known bugs

* `INIViewer -! -o file.ini <all_you_want> > file.ini` does not write anything to the file
