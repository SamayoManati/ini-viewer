#! /usr/bin/env python3

# TODO ajouter le support des commentaires en début de section (commentaires de section (INISection))
# TODO ajouter le support des commentaires en fin de ligne (commentaires de lignes (INIKey))
# TODO ajouter le support des commentaires en début de fichier (commentaires de fichier (INIHandler)
# TODO make the multilines values comments work

import re

__all__ = ['INIHandler', 'INISection', 'INIKey']

class INIKey:
	def __str__(self):
		n = self.get_name()
		v = self.get_value().replace('\n', '\n\t')
		delimiter = self.get_parent().get_parent().get_option('delimiters').split('|')[0]
		delimiter = ' ' + delimiter + ' '
		comment = ''
		if self.__comment != '' and self.__comment != None:
			cp = self.get_parent().get_parent().get_option('comment_prefixes').split('|')[0]
			lc = 0
			for i in self.__comment.split('\n'):
				if lc > 0:
					comment += '\n'
					comment += ' ' * len(n + delimiter + v)
				comment += ' ' + cp + ' ' + i
				lc += 1
		return n + delimiter + v + comment

	def __init__(self, name, value, section, comment = ''):
		self.__name = name
		self.__value = value
		self.__parent = section
		self.__comment = comment

	def get_section(self):
		"""Return a reference to the section of the key."""
		return self.__parent

	def set_value(self, value):
		"""Change the value of the key.
		value : (str)"""
		self.__value = str(value)
		return None

	def get_value(self):
		return self.__value

	def get_name(self):
		return self.__name

	def set_name(self, name):
		"""Change the name of the key.
		name : (str)

		[return]
		 - True if the name is valid
		 - False otherwise
		"""
		if self.get_parent().get_parent().get_regex('key_name').match(name):
			self.__name = name
			return True
		else:
			return False

	def get_comment(self):
		return self.__comment

	def set_comment(self, comment):
		self.__comment = comment
		return None

	def get_parent(self):
		"""Return a reference to the section."""
		return self.__parent

class INISection:
	def __setitem__(self, key, value):
		if self.has_key_by_name(key):
			self.get_key_by_name(key).set_value(value)
		else:
			self.create_key(key, value)

	def __contains__(self, item):
		return self.has_key_by_name(item)

	def __str__(self):
		r = '['
		r += self.get_name()
		r += ']'
		r += '\n'
		if self.__comment != '' and self.__comment != None:
			cp = self.get_parent().get_option('comment_prefixes').split('|')[0]
			for i in self.__comment.split('\n'):
				r += cp + ' ' + i + '\n'
		for i in self.get_keys():
			r += str(i)
			r += '\n'
		r += '\n'
		return r

	def __delitem__(self, key):
		if self.has_key_by_name(key):
			r = self.get_key_by_name(key)
			self.remove_key(key)
			return r
		else:
			return None

	def __getitem__(self, item):
		if self.has_key_by_name(item):
			return self.get_key_by_name(item)
		else:
			return None

	def __init__(self, name, inihandler, comment=''):
		self.__name = name
		self.__keys = []
		self.__parent = inihandler
		self.__comment = comment

	def get_ini(self):
		"""Return a reference to the containing INIHandler"""
		return self.__parent

	def get_name(self):
		return self.__name

	def set_name(self, name):
		"""Try to change the section's name.
		name : (str) the new name.

		[return]
		 - True if the section name is valid
		 - False otherwise
		"""
		if self.get_parent().get_regex('section').match('[' + name + ']'):
			self.__name = name
			return True
		else:	return False

	def get_comment(self):
		return self.__comment

	def set_comment(self, comment):
		self.__comment = comment
		return None

	def get_keys(self):
		"""Return a list of all keys (INIKey) in the section."""
		return self.__keys

	def get_keys_by_name(self, name):
		"""Return a list of keys in the section with the given name.
		name : (str) The name of the keys.

		[return]
		list of INIKey"""
		return [i for i in self.__keys if i.get_name() == name]

	def get_key_by_name(self, name):
		"""Return the first key with the given name.
		name : (str)

		@return INIKey or None"""
		try:
			return [i for i in self.__keys if i.get_name() == name][0]
		except IndexError:
			return None

	def get_keys_by_value(self, value):
		"""Return a list of keys in the section with the given value.
		value : (str) The value of the keys.

		[return]
		list of INIKey"""
		return [i for i in self.__keys if i.get_value() == value]

	def get_key_by_value(self, value):
		"""Return the first key in the section with the given value.
		value : (str)

		@return INIKey or None"""
		try:
			return [i for i in self.__keys if i.get_name() == value][0]
		except IndexError:
			return None

	def has_key_by_name(self, name):
		"""Return if the section has a key with the given name.
		name : (str) The name of the key.

		[return]
		bool"""
		return not self.get_keys_by_name(name) == []

	def has_key_by_value(self, value):
		"""Return if the section has a key with the given value.
		value : (str) The value.

		[return]
		bool"""
		return not self.get_keys_by_value(value) == []

	def rename_keys(self, kname, nkname):
		"""Rename all keys with the given name.
		kname : (str) The current key name
		nkname : (str) The new key name

		@return True if the new name is valid and at least one key exists, False otherwise"""
		if self.get_parent().get_regex('key_name').match(nkname):
			k = self.get_keys_by_name(kname)
			for i in k:
				i.set_name(nkname)
			return k != []
		else:
			return False

	def rename_key(self, kname, nkname):
		"""Rename the first key in the section with the given name.
		kname : (str) the current key name
		nkname : (str) the new key name

		@return True if the new name is valid and the key exists, False otherwise"""
		if self.get_parent().get_regex('key_name').match(nkname):
			s = self.get_key_by_name(kname)
			if s != None:
				s.set_name(nkname)
				return True
			else:
				return False
		return False

	def remove_keys(self, kname):
		"""Remove all keys with the given name."""
		for k, v in enumerate(self.__keys):
			if self.__keys[k].get_name() == kname:
				self.__keys.pop(k)
		return None

	def remove_key(self, kname):
		"""Remove the first key with the given name.
		kname : (str)

		@return None"""
		for k, v in enumerate(self.__keys):
			if self.__keys[k].get_name() == kname:
				self.__keys.pop(k)
				return None
		return None

	def create_key(self, kname, value):
		"""Create a new key (fails if the key already exists)
		kname : (str)
		value : (str)

		@return True if the key has been created, False otherwise"""
		if self.has_key_by_name(kname):
			return False
		self.__keys.append(INIKey(kname, value, self))
		return True

	def empty(self):
		self.__keys = []
		return None

	def get_parent(self):
		"""Return a reference to the ini handler."""
		return self.__parent

"""
An INI datas manager.

The sections names can contains the following characters:
 - uppercases and lowercases letters
 - numbers
 - spaces
 - dots
 - parenthesis
 - curly braces
 - scores
 - underscores
 - at
 - slashs
 - backslasks
 - plus
 - asterisks
They cannot contains double-dots (:) or equal-sign (=).

The options are the followings :
 - allow_no_value : (bool) Allow the keys not to contains any value, nor delimiter (not implemented yet)
 - delimiters : (str) The list of delimiters used to separate key's name and value. Must be of type : <delimiter1>|<delimiter2>|<...>
 - comment_prefixes : (str) The list of prefixes for comments. See delimiters for the format
 - empty_lines_in_values : (bool) Allow the multilines values.

[MULTILINES VALUES]
The multilines values are values which takes more than 1 line. The continuation lines must be indented.

	[section]
	key = blablabla
		blablabla
		blablabla
		
		blablabla
	another_key = blablabla
		blablabla
		blablabla
"""
class INIHandler:
	def __str__(self):
		r = ''
		if self.__comment != '' and self.__comment != None:
			cp = self.__o_cp.split('|')[0]
			for i in self.__comment.split('\n'):
				r += cp + ' ' + i + '\n'
		for i in self.__content:
			r += str(i)
		return r

	def __contains__(self, item):
		return self.has_section(item)

	def __delitem__(self, key):
		if self.has_section(key):
			r = self.get_section_by_name(key)
			self.remove_section(key)
			return r
		else:
			return None

	def __getitem__(self, item):
		if self.has_section(item):
			return self.get_section_by_name(item)
		else:
			return None

	def __init__(self, allow_no_value = True, delimiters = '=|:', comment_prefixes = ';|#', empty_lines_in_values = True, comment=''):
		# new
		self.__o_anv = allow_no_value
		self.__o_d = delimiters # must be a string at re regex format, without the () (ex : ":|=" (-> "(:|=)"), or "bonjour|s" (-> "(bonjour|s)"))
		self.__o_cp = comment_prefixes
		self.__o_eliv = empty_lines_in_values
		self.__content = []
		self.__comment = ''
		self.__rec_section = re.compile(self.get_regex_pattern('section'))
		self.__rec_key = re.compile(self.get_regex_pattern('key'))
		self.__rec_key_name = re.compile(self.get_regex_pattern('key_name'))
		self.__rec_continuation = re.compile(self.get_regex_pattern('continuation'))
		self.__rec_comment = re.compile(self.get_regex_pattern('comment'))
		self._fp = ''

	def read_string(self, string):
		"""Read INI datas from a string and append them to the existent datas.
		string : (str) the INI datas."""
		s = string.split('\n')
		linesCounter = 0
		cur_sec = ''
		cur_key = ''
		for line in s:
			issecname = self.get_regex('section').match(line)
			iskey = self.get_regex('key').match(line)
			iscontinuation = self.get_regex('continuation').match(line)
			iscomment = self.get_regex('comment').match(line)
			if issecname != None:
				# the line is a section name definition
				cur_sec = issecname.group('name')
				self.create_section(cur_sec)
			elif iskey != None:
				# the line is a key
				cur_key = iskey.group('name').rstrip() # rstrip to erase last space
				val = iskey.group('content').lstrip()
				valnocomment = val.split('(' + self.__o_cp + ')')[0]
				self.create_key(cur_sec, cur_key, valnocomment)
				comment = self.get_regex('comment').match(val)
				if comment != None:
					self.get_section_by_name(cur_sec).get_key_by_name(cur_key).set_comment(comment.group('comment'))
			elif iscontinuation != None:
				# the line is a previous line value continuation
				v = iscontinuation.group('content').lstrip() # lstrip to erase first space around the delimiter
				v2 = self.get_section_by_name(cur_sec).get_key_by_name(cur_key).get_value()
				self.get_section_by_name(cur_sec).get_key_by_name(cur_key).set_value(v2 + '\n' + v)
			elif iscomment != None:
				if cur_sec == '':
					# this is a file comment
					c = iscomment.group('comment').lstrip() # lstrip to erase first space after the comment sign
					self.__comment += c + '\n'
			linesCounter += 1
		return None

	def read_dict(self, dic):
		"""Read the dic dict datas and append them to the ini datas.
		dic : (dict)"""
		for cur_sec in dic:
			self.create_section(str(cur_sec))
			for cur_key in dic[cur_sec]:
				self.get_section_by_name(str(cur_sec)).create_key(str(cur_key), str(dic[cur_sec][cur_key]))
		return None

	def read_file(self, filepath):
		"""Open an INI file."""
		# ouvrir le fichier        X
		# le lire avec read_string X
		# envoyer à l'UI le changement de titre vers 'INI Viewer - ' + CURRENTFILEPATH.split('/')[-1] (PEUT ETRE PAS)
		# update l'affichage de l'ui (PEUT ETRE PAS)
		with open(filepath, 'r') as fp:
			self.read_string(fp.read())
		self._fp = filepath
		return None

	def write(self, filepath):
		"""Write the INI to a file."""
		with open(filepath, 'w') as fp:
			fp.write(str(self))
		return None

	def create_section(self, sname):
		"""Create a section.
		sname : (str) the name of the section.

		[return]
		- True if the section has been created
		- False otherwise"""
		self.__content.append(INISection(sname, self))
		return True

	def remove_section(self, sname):
		"""Remove a section.

		@return True if the section has been deleted, False otherwise."""
		r = False
		for k, v in enumerate(self.__content):
			if self.__content[k].get_name() == sname:
				self.__content.pop(k)
				r = True
		return r

	def rename_section(self, sname, nsname):
		"""Change the name of a section.
		sname : (str) the current section name
		nsname : (str) the new section name

		<return>
		True if the section was renamed, False otherwise."""
		r = False
		if not self.get_regex('section').match('[' + nsname + ']'):
			return r
		s = self.get_sections_by_name(sname)
		for i in s:
			i.set_name(nsname)
			r = True
		return r

	def clone_section(self, sname, nsname):
		pass

	def get_sections(self):
		"""Return the list of the sections (INISection)."""
		return self.__content

	def get_sections_by_name(self, name):
		"""Return a list of sections with the given name.
		name : (str)

		<RETURN>
		[INISection]
		"""
		return [i for i in self.__content if i.get_name() == name]

	def get_section_by_name(self, name):
		"""Return the first section with the given name.
		name : (str)

		@return INISection or None"""
		try:
			return [i for i in self.__content if i.get_name() == name][0]
		except IndexError:
			return None

	def has_section(self, sname):
		"""Return if a section exists with the given name.
		sname : (str)

		<RETURN>
		bool"""
		return not self.get_sections_by_name(sname) == []

	def create_key(self, sname, kname, kvalue):
		"""Create a new key into each sections with the sname name.
		sname : (str) the name of the sections
		kname : (str) the name of the key
		kvalue : (str) the value of the key

		@return True if at least one key has been created, False otherwise"""
		r = False
		for i in self.get_sections_by_name(sname):
			r = r or i.create_key(kname, kvalue)
		return r

	def empty(self):
		"""Empty the datas"""
		self.__content = []
		return None

	def get_comment(self):
		return self.__comment

	def set_comment(self, comment):
		self.__comment = comment
		return None

	def get_regex_pattern(self, subject):
		if subject == 'section':		return '^\s*\[(?P<name>[a-zA-Z0-9 .(){}\-_@/\\+*]+)\]\s*$'
		elif subject == 'key':			return '^(?P<name>[a-zA-Z0-9 .(){}\-_@/\\+*]+)(' + self.__o_d + ')(?P<content>.*)$'
		elif subject == 'key_name':		return '^(?P<name>[a-zA-Z0-9 .(){}\-_@/\\+*]+)$'
		elif subject == 'continuation':	return '^\s+(?P<content>.*)$'
		elif subject == 'comment':		return '(' + self.__o_cp + ')(?P<comment>.*)$'
		else:							return ''

	def get_regex(self, subject):
		if subject == 'section':		return self.__rec_section
		elif subject == 'key':			return self.__rec_key
		elif subject == 'key_name':		return self.__rec_key_name
		elif subject == 'continuation': return self.__rec_continuation
		elif subject == 'comment':		return self.__rec_comment
		else:							return None

	def get_option(self, option):
		"""Return the value of an option.
		option : (str)"""
		if option == 'allow_no_value':				return self.__o_anv
		elif option == 'delimiters':				return self.__o_d
		elif option == 'comment_prefixes':			return self.__o_cp
		elif option == 'empty_lines_in_values':		return self.__o_eliv
		else:										return ''

	@staticmethod
	def create_section_iid(sname):
		"""Create a section IID for the tkinter.ttk.Treeview component
		sname : (str) the name of the section

		see also : INIHandler.section_from_iid"""
		return sname

	@staticmethod
	def create_key_iid(sname, kname):
		"""Create a section IID for the tkinter.ttk.Treeview component
		sname : (str) the name of the section
		kname : (str) the name of the key

		see also : INIHandler.key_from_iid"""
		return INIHandler.create_section_iid(sname) + ':' + kname

	@staticmethod
	def section_from_iid(iid):
		"""Return a section name from its IID
		iid : (str) the IID

		see also : INIHandler.create_section_iid"""
		return iid.strip().split(':')[0]

	@staticmethod
	def key_from_iid(iid):
		"""Return a key name from its IID
		iid : (str) the IID

		see also : INIHandler.create_key_iid"""
		s = iid.strip().split(':')
		if len(s) >= 2:
			return s[1]
		else:
			return ''


if __name__ == '__main__':
	i = INIHandler()
	i.create_section('section')
	i.get_section_by_name('section').create_key('maque', '74')
	i.get_section_by_name('section').get_key_by_name('maque').set_comment('yolo swag lol lol')
	i.read_string('''[section2]
key = multiline
 value
  you don\'t
   think ?
key2 = true''')
	i.read_dict({'section3':{'kk': 'value\nwith\nlines'}})
	i['section']['ablablabla'] = 'true'
	print(i)
