#! /usr/bin/env python3

import configparser
import os.path

from ini import INIHandler

__all__ = ['Options', 'GVars', 'make_INIHandler']

class Options:
	"""This class contains all the options"""

	options_storage_filename = 'options.ini'
	allow_no_value = True
	delimiters = ('=', ':')
	comment_prefixes = ('#', ';')
	inline_comment_prefixes = None
	strict = True
	empty_lines_in_values = True

	@staticmethod
	def load(filename = None):
		if filename == None:
			filename = os.path.dirname(os.path.realpath(__file__)) + '/' + Options.options_storage_filename
		with open(filename, 'r') as fp:
			ini = configparser.ConfigParser()
			ini.read_file(fp)
			Options.allow_no_value = ini.getboolean('INI', 'AllowNoValue', fallback=Options.allow_no_value)
			Options.delimiters = tuple(ini.get('INI', 'Delimiters').replace('"', '').replace("'", '').split(' ')) if ini.has_option('INI', 'Delimiters') else Options.delimiters
			Options.comment_prefixes = tuple(ini.get('INI', 'CommentPrefixes').replace('"', '').replace("'", '').split(' ')) if ini.has_option('INI', 'CommentPrefixes') else Options.comment_prefixes
			inlinecommentprefixes = tuple(ini.get('INI', 'InlineCommentPrefixes').replace('"', '').replace("'", '').split(' ')) if ini.has_option('INI', 'InlineCommentPrefixes') else Options.inline_comment_prefixes
			Options.inline_comment_prefixes = inlinecommentprefixes if inlinecommentprefixes!=('',) else None
			Options.strict = ini.getboolean('INI', 'Strict', fallback=Options.strict)
			Options.empty_lines_in_values = ini.getboolean('INI', 'EmptyLinesInValues', fallback=Options.empty_lines_in_values)
		return None

def make_INIHandler():
	return INIHandler(allow_no_value=Options.allow_no_value,
			delimiters=Options.delimiters,
			comment_prefixes=Options.comment_prefixes,
			inline_comment_prefixes=Options.inline_comment_prefixes,
			strict=Options.strict,
			empty_lines_in_values=Options.empty_lines_in_values,
			interpolation=configparser.Interpolation())

class GVars:
	"""Contains all the global variables."""

	filetypes = (('INI Configuration files', '*.ini *.inf *.reg'), ('All files', '*.*'))
	ctxmenu = False # context menu
	climode = False # console-only mode

	ini = make_INIHandler()
