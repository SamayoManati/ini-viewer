from tkinter import *
from tkinter.ttk import *

class AboutBox:
	__text = ''

	def event_btnok(self):
		self.top.destroy()
		return None

	def __init__(self, root, text = 'A simple INI configuration files viewer.\n\nProgrammation : Soni Tourret <https://gitlab.com/SamayoManati>\nVersion : 1.0'):
		self.__text = text
		self.top = Toplevel(root)
		self.top.title('About')
		self.top.columnconfigure(0, weight=1)
		self.top.rowconfigure(0, weight=1)
		self._f_content = Frame(self.top)
		self._f_content.columnconfigure(0, weight=1)
		self._f_content.rowconfigure(0, weight=1)
		self._f_message = Label(self._f_content, text=self.__text)
		self._f_btnok = Button(self._f_content, text='OK', command=self.event_btnok)
		self.draw()

	def draw(self):
		self._f_message.configure(text=self.__text)
		self._f_content.grid(row=0, column=0, sticky='snew')
		self._f_message.grid(row=0, column=0, sticky='snew')
		self._f_btnok.grid(row=1, column=0, sticky='snew')
		return self

	def setText(self, text):
		self.__text = text
		self.draw()
		return self

	def getText(self):
		return self.__text
